/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ascensore;

/**
 *
 * @author Fabio Moscariello
 */
public class Ascensore {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        utilizzaAscensore(5,2);
    }

    public static void utilizzaAscensore(int pianoDiPartenza, int pianoDiArrivo) {
        int pianoMax=5;
        if(pianoDiPartenza<pianoDiArrivo)        
        while(pianoDiPartenza<=pianoDiArrivo){
            System.out.println("Piano " + pianoDiPartenza++ +" Salgo");
            
        }else{
            while(pianoDiPartenza>=pianoDiArrivo)
            System.out.println("Piano "+ pianoDiPartenza-- + " Scendo");
        }
        System.out.println("Sono arrivato");
    }
    
}
