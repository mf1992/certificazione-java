/*
Exercise 5-3

1. Create a for loop that iterates through the items array,
     displaying each element. Precede the list of elements with 
     the message, "Items purchased: ".
 */
package esercizio10;

/**
 *
 * @author Fabio Moscariello
 */
public class Esercizio10 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String elements[] = {"Shirt", "Scarf", "Polo"};
       for(String items:elements){
           System.out.println("Items purchased: "+items);
           
       }
    }
    
}
