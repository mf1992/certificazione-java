/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libreria;

/**
 *
 * @author Fabio Moscariello
 */
public class LibroIllustrato extends Libro{
    private int numeroDisegni;
    public LibroIllustrato()
    {
    super();
    numeroDisegni=1;
    }
    public LibroIllustrato(Autore autore, int anno, double prezzo, String titolo,int numeroDisegni)
    {
    super(autore, anno, prezzo, titolo);
    this.numeroDisegni=numeroDisegni;
    }

    /**
     * @return the numeroDisegni
     */
    public int getNumeroDisegni() {
        return numeroDisegni;
    }

    /**
     * @param numeroDisegni the numeroDisegni to set
     */
    public void setNumeroDisegni(int numeroDisegni) {
        this.numeroDisegni = numeroDisegni;
    }
}
