/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libreria;

/**
 *
 * @author Fabio Moscariello
 */
public class LibroScolastico extends Libro{
    private String materia;
    public LibroScolastico(){
    super();
    materia="prova";
    }
    public LibroScolastico(Autore autore,int anno,double prezzo,String titolo,String materia){
           
        super(autore, anno, prezzo, titolo);
        this.materia=materia;
}

    /**
     * @return the materia
     */
    public String getMateria() {
        return materia;
    }

    /**
     * @param materia the materia to set
     */
    public void setMateria(String materia) {
        this.materia = materia;
    }
   
}
