/*
Exercise 5-2

1. Declare a String array and initialize it with 4 elements.
     Each element represents a different item description ("Shirt", for instance).
2. Change message to show how many items the customer wants to purchase 
     (Use the length property of the items array).
3. Print just one element of the items array.  
     What happens if you use index number 4?
 */
package esercizio9;

/**
 *
 * @author Fabio Moscariello
 */
public class Esercizio9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String elements[] = {"Shirt", "Scarf", "Polo"};
        boolean outOfStock = false;
        double price = 10, tax = 120;
        int quantity = 0;
        double total;
        String custName = "Mary Smith";
        quantity=elements.length;
        String message = custName + " wants to purchase " + quantity + " "+elements[2];
        if (quantity > 1) {
            System.out.println(message + "s");
            
        }
        if (outOfStock == false) {
            total = price * quantity * tax;
            System.out.println("Total cost with tax is: " + total);
        } else {
            System.out.println("The item is unavailable");
        }

    }

}
