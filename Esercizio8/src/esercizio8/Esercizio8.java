/*Exercise 5-1:

1. Declare a boolean, outOfStock, and initialize it to true.
2. Use an if statement to test the value of quantity.  
    If it is > 1, concatenate an 's' to message so that it indicates multiple items.
3. Use an if | else statement that tests the value of outOfStock.
    If true, let the user know the item is unavailable.
    If false, print the message variable, then print the total cost with tax.
4. Run with outOfStock = true and then again with outOfStock = false.*/
package esercizio8;

/**
 *
 * @author Fabio Moscariello
 */
public class Esercizio8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    boolean outOfStock=false;
    double price=10,tax=120;
        int quantity=5;
        double total;
        String message="Mary Smith wants to purchase "+quantity+" Shirt";
        if(quantity>1)
        {
            System.out.println(message+"s");    
        }
        if(outOfStock==false){
        total=price*quantity*tax;
        System.out.println("Total cost with tax is: "+total);
        }else{
            System.out.println("The item is unavailable");
        }
        
        
        
    
    }

}
