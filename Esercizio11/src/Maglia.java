/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fabio Moscariello
 */
public class Maglia {

    public Maglia(String taglia, String colore, double prezzo) {
        this.taglia = taglia;
        this.colore = colore;
        this.prezzo = prezzo;
    }

    public Maglia() {
        taglia = "S";
        colore = "NOCOLOR";
        prezzo = 0.0;
    }

    /**
     * @return the taglia
     */
    public String getTaglia() {
        return taglia;
    }

    /**
     * @param taglia the taglia to set
     */
    public void setTaglia(String taglia) {
        this.taglia = taglia;
    }

    /**
     * @return the colore
     */
    public String getColore() {
        return colore;
    }

    /**
     * @param colore the colore to set
     */
    public void setColore(String colore) {
        this.colore = colore;
    }

    /**
     * @return the prezzo
     */
    public double getPrezzo() {
        return prezzo;
    }

    /**
     * @param prezzo the prezzo to set
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    private String taglia;
    private String colore;
    private double prezzo;
}
