/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises9;

import exercises8.*;

/**
 *
 * @author Fabio Moscariello
 */
public class Item {

    private boolean outOfStock;
    private char color;
    private static String ssn;
    public Item(boolean outOfStock, char color) {
        this.outOfStock = outOfStock;
        this.color = color;
    }
    public Item(){
    
    
    }
    /**
     * @return the outOfStock
     */
    public boolean isOutOfStock() {
        return outOfStock;
    }

    /**
     * @param outOfStock the outOfStock to set
     */
    public void setOutOfStock(boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    /**
     * @return the color
     */
    public char getColor() {
        return color;
    }

    /**
     * @param Color the color to set
     */
    public boolean setColor(char code) {
        boolean check;
        check=(code != ' ') ? controlColor(code) : false;
        return check;
    }
    
        private boolean controlColor(char color){
        this.color=color;
        return true;   
    }
}

