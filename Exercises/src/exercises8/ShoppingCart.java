package exercises8;

/*Exercise 8-1

In the Item class:
1. Declare a setColor method that takes a char as an argument (a color code)
     and returns a boolean.
     Return false if the colorCode is ' '.
     Otherwise, assign the colorCode to the color field and return true.

In the ShoppingCart class:
2. Call the setColor method on item1.  If it returns true,
     print out item1.color.  If it returns false, print an
     invalid color message.
3. Test the method with both a valid color and an invalid one.
/*
*/
/**
 *
 * @author Fabio Moscariello
 */
public class ShoppingCart {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String elements[] = {"Shirt", "Scarf", "Polo"};
        boolean outOfStock = false;
        double price = 10, tax = 120;
        int quantity = 0;
        double total;
        String custName = "Mary Smith";
        String firstName;
        String lastName;
        int spaceIdx;
        
        quantity=elements.length;
        String message = custName + " wants to purchase " + quantity + " "+elements[2];
        if (quantity > 1) {
            System.out.println(message + "s");
            
        }
        if (outOfStock == false) {
            total = price * quantity * tax;
            System.out.println("Total cost with tax is: " + total);
        } else {
            System.out.println("The item is unavailable");
        }
        spaceIdx=custName.indexOf(" ");
        firstName=custName.substring(0, spaceIdx);
        StringBuilder sb= new StringBuilder(firstName);
        lastName=custName.substring(spaceIdx);
        sb.append(lastName);
        System.out.println(sb);
    }

}
