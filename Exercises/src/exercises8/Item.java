/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercises8;

/**
 *
 * @author Fabio Moscariello
 */
public class Item {

    private boolean outOfStock;
    private String color;

    public Item(boolean outOfStock, String Color) {
        this.outOfStock = outOfStock;
        this.color = Color;
    }

    /**
     * @return the outOfStock
     */
    public boolean isOutOfStock() {
        return outOfStock;
    }

    /**
     * @param outOfStock the outOfStock to set
     */
    public void setOutOfStock(boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param Color the color to set
     */
    public boolean setColor(char code) {
        return (code!='') ? true this.color=code; :false;  
        return false;
    }

}
