package exercises11.exercises1;

// import statements here:

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;




public class ShoppingCart {
    public static void main(String[] args){
	// Declare a LocalDateTime object, orderDate
        LocalDateTime orderDate;
       
        
	// Initialize the orderDate to the current date and time. Print it.
        orderDate=LocalDateTime.now();
        System.out.println("orderDate:" +orderDate);

	// Format orderDate using ISO_LOCAL_DATE; Print it.
        StringBuilder formatter= new StringBuilder();
        formatter.append(orderDate.format(DateTimeFormatter.ISO_DATE));
        System.out.println("orderDate Formatter: "+formatter);
        
        

    }
}