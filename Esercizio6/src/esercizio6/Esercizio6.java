/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercizio6;

/**
 *
 * @author Fabio Moscariello
 */
public class Esercizio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] m = new int[3];
        int indice;
        m[0] = 100;
        m[1] = 20;
        m[2] = 3;

        if (m[1] > m[0] && m[1] < m[2]) {
            indice = 2;
            System.out.println("Il più grande è " + m[2] + " e l'indice è " + indice);

        } else if (m[0] > m[2] && m[0] < m[1]) {
            indice = 1;
            System.out.println("Il più grande è " + m[1] + " e l'indice è " + indice);
        } else {
            indice = 0;
            System.out.println("Il più grande è " + m[0] + " e l'indice è " + indice);
        }

    }

}
