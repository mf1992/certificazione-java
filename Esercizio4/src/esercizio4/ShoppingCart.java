/*Exercise 4-2

1. Declare and initialize numeric fields. Include price and tax (double), 
     quantity (int). Also declare a double called total, but do not initialize it.
2. Change the message variable to include quantity 
      (example: "Mary Smith wants to purchase 1 Shirt.")
3. Calculate total by multiplying price * quantity * tax.
4. Print a message showing the total cost. 
      (example:  "Total cost with tax is: 25.78.") 
*/
package esercizio4;

/**
 *
 * @author Fabio Moscariello
 */
public class ShoppingCart {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double price=10,tax=120;
        int quantity=5;
        double total;
        String message="Mary Smith wants to purchase "+quantity+" Shirt";
        total=price*quantity*tax;
        System.out.println("Total cost with tax is: "+total);
    }
    
}
